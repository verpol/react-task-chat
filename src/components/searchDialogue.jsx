import React from 'react';
import './style.css'

class SearchDialogue extends React.Component {
	render() {
		return (
			<div className="search-block">
				<input className="search" type="text" placeholder="Search" />
			</div>
		);
	}
}

export default SearchDialogue;