import React from 'react';
import './style.css'
import ItemDialogue from "./itemDialogue"
import dialoguesData from "./dialoguesData";

class ListDialogues extends React.Component {

	renderDialogue = dialogue => (
		<ItemDialogue
			onClick={this.clickHandler(dialogue.name)}
			name={dialogue.name}
			date={dialogue.date}
		    photo={dialogue.photo}
		    text={dialogue.text}
		    key={dialogue.name} />
	);

	clickHandler = name => () => {
		this.props.setData(name)
	};

	render() {
		return (
			<div>
				{dialoguesData.map(this.renderDialogue)}
			</div>
		);
	}
}

export default ListDialogues;