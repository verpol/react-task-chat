import React from 'react';
import './style.css';
import attach from './img/attach.png'

class NewMessage extends React.Component {
	render() {
		return (
			<div className="new-message footer">
				<div className="new-message-attach">
					<img src={attach} alt="attach" />
				</div>
				<input type="text" className="new-message-enter" placeholder="Type a message" />
			</div>
		);
	}
}

export default NewMessage;