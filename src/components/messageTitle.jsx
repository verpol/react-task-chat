import React from 'react';
import './style.css'
import bell from './img/bell.png';
import clock from './img/clock.png'
import trash from './img/trash.png'

class Title extends React.Component {
	render() {
		return (
			<div className="dialogue-name-block">
				<div className="dialogue-name">{this.props.text}</div>
				{this.props.text && (
					<div className="dialogue-control">
						<img src={bell} alt={"bell"} />
						<img src={clock} alt={"clock"} />
						<img src={trash} alt={"trash"} />
					</div>
				)}
			</div>
		);
	}
}

export default Title;