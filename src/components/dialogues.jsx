import React from 'react';
import './style.css'
import SearchDialogue from "./searchDialogue";
import ListDialogues from "./listDialogues";
import NewDialogue from './newDialogue';

class Dialogues extends React.Component {
	render() {
		return (
			<div>
				<div className="content">
					<SearchDialogue />
					<ListDialogues setData={this.props.setData} />
				</div>
				<div className="footer">
					<NewDialogue />
				</div>
			</div>
		);
	}
}

export default Dialogues