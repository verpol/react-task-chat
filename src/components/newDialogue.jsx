import React from 'react';
import './style.css';
import plus from './img/plus.png';
import phone from './img/phone.png';

class NewDialogue extends React.Component {
	render() {
		return (
			<div className="new-dialogue">
				<div>
					<img src={plus} alt="add dialogue" />
				</div>
				<div className="phone">
					<img src={phone} alt="telephone" />
					<span className="symbol">&#9650;</span>
				</div>
			</div>
		);
	}
}

export default NewDialogue;