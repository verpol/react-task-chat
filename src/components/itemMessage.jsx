import React from 'react';
import './style.css'

class ItemMessage extends React.Component {
	render() {
		return (
			<div className="list-of-messages">
				<div className="message-item">
					<div>
						<img className="photo" src={this.props.photo} alt="man" />
					</div>
					<div className="message-info">
						<div className="message-name-date">
							<p>{this.props.name}</p>
							<p className="date">{this.props.date}</p>
						</div>
						<p className="message-text">{this.props.text}</p>
					</div>
				</div>
			</div>
		);
	}
}

export default ItemMessage;