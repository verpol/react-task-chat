import React from 'react';
import './style.css'
import Messages from "./messages";
import Dialogues from "./dialogues";

class Chat extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			currentChat: null,
		};
	}

	setData = (data) => {
		this.setState({
			currentChat: data
		});
	};

	render() {
		return (
			<div className="container">
				<div className="dialogues">
					<Dialogues setData={this.setData} />
				</div>
				<div className="messages">
					<Messages currentChat={this.state.currentChat} />
				</div>
			</div>
		);
	}
}

export default Chat;