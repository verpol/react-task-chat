import React from 'react';
import './style.css';
import ItemMessage from "./itemMessage";
import messageData from "./messageData.json";

class ListMessages extends React.Component {

	renderMessage = messageData => (
		<ItemMessage name={messageData.name}
		             date={messageData.date}
		             photo={messageData.photo}
		             text={messageData.text}
		             key={messageData.name} />
	);

	render() {
		console.log(messageData);
		const currentChatMessages = messageData.find(message => message.name === this.props.currentChat);

		if (!this.props.currentChat) {
			return null;
		}

		return (
			<div>
				{currentChatMessages.messages.map(this.renderMessage)}
			</div>
		);
	}
}

export default ListMessages;