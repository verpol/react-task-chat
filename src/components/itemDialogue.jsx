import React from 'react';
import './style.css';

class ItemDialogue extends React.Component {

	render() {
		return (
			<div onClick={this.props.onClick}>
				<div className="dialogue-item">
					<div>
						<img className="photo" src={this.props.photo} alt="avatar" />
					</div>
					<div className="about-dialogue">
						<div className="dialogue-name-date">
							<p>{this.props.name}</p>
							<p className="date">{this.props.date}</p>
						</div>
						<p className="dialogue-short-text">{this.props.text}</p>
					</div>
				</div>
			</div>
		);
	}
}

export default ItemDialogue;