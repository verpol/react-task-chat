import React from 'react';
import './style.css'
import Title from "./messageTitle";
import ListMessages from "./listMessages";
import NewMessage from "./newMessage";

class Messages extends React.Component {
	render() {
		return (
			<div>
				{this.props.currentChat && (
				<div className="content">
					<Title text={this.props.currentChat} />
					<ListMessages currentChat={this.props.currentChat}/>
				</div>
				)}
				{this.props.currentChat && (
					<div className="footer">
						<NewMessage/>
					</div>
				)}
			</div>
		);
	}
}

export default Messages;